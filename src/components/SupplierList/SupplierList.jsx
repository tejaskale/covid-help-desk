import React , { useState, useEffect, Fragment } from 'react'
import SupplierCard from "../SupplierCard/SupplierCard"
import "./Supplier-list.css"


function SupplierList() {
const HEADERS = {
  'Content-Type': 'application/json',
  'X-I2CE-ENTERPRISE-ID': 'dave_vs_covid',
  'X-I2CE-USER-ID': 'ananth+covid@i2ce.in',
  'X-I2CE-API-KEY': '0349234-38472-1209-2837-3432434',
};
const [suppliers, setSuppliers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const[isFirstPage,setFirstPage] = useState(true)
  const[isLastPage,setLastPage] = useState(false)


  async function Suppliers() {
    try {
      const response = await fetch(`https://test.iamdave.ai/list/supply?_page_number=${currentPage}`, {
        headers: HEADERS,
      });
      const data = await response.json();
      setSuppliers([data.data]);
      if(data.is_first){
        setFirstPage(true)
      }
      else if(data.is_last){
        setLastPage(true)
      }
      else{
        setLastPage(false)
        setFirstPage(false)
      }
      
    } catch (error) {
      console.log(error);
    }
  }

  
  useEffect(() => {
    Suppliers();
  }, [currentPage]);

  function handleNextPage() {
    setCurrentPage(currentPage + 1);
  }

  function handlePrevPage() {
    setCurrentPage(currentPage - 1);
  }
  return (
  <Fragment>
      <div className="supplier-list">
      {
        suppliers.map((item,index)=>{
          return (
            item.map((supplier,sequence)=>{
              return(
                <SupplierCard
          key={sequence}
          sequence={sequence}
          category={supplier.category}
          channel={supplier.channel}
          requestDescription={supplier.request_description}
          contactNumbers={supplier.contact_numbers}
          state={supplier.state}
          district={supplier.district}
          sourceTime={supplier.source_time}
        />
              )
            })
          )
        })
      }      
    </div>
    <button className={isFirstPage? "btn-first-page" : "btn"} onClick={handlePrevPage}>Previous Page</button>
    <button className={isLastPage? "btn-last-page" : "btn"} onClick={handleNextPage}>Next Page</button>
  </Fragment>
  );

}

export default SupplierList;
