import React from 'react';
import "./supplier-card.css";
import twitterLogo from "../../images/logo-twitter.png"
import other from "../../images/other-logo.png"

function SupplierCard (props) {
return (
  <div key={props.sequence} className="supplier-card">
    <h1>{props.category}</h1>
    {/* <p className={props.channel === "twitter" ? "twitter" : props.channel === "facebook"? "facebook" : "instagram"} >{props.channel}</p> */}

  
        {props.channel === "twitter" ? <div className='twitter'> <img src={twitterLogo} alt = "twitter logo"/> <h3>{props.channel}</h3> </div> : <div className='twitter'> <img src={other} alt = "logo"/> <h3>{props.channel}</h3> </div>}
        
    <p>{props.requestDescription}</p>
    <p>{props.contactNumbers.length > 1 ? <input type="tel" readOnly className='phone-num' value={props.contactNumbers[0]} id="" />  : <input type="tel" readOnly className='phone-num' value={props.contactNumbers} id="" />  }</p>
    {/* {props.contactNumbers.length > 1 ? props.contactNumbers.map((contact,index)=>{<p key={index} >{contact}</p>}) : <p>{props.contactNumbers}</p>} */}
    <div><h3 className='location'>{props.state} </h3>
    <h3 className='location'>{props.district}</h3>
    </div>
    
    <p>{props.sourceTime}</p>
  </div>
);
}

export default SupplierCard;