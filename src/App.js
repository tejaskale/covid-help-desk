import logo from './logo.svg';
import './App.css';
import SupplierList from "./components/SupplierList/SupplierList"

function App() {
  return (
    <div className="App">
      <div className="app">
      <h1>Supplier List</h1>
      <SupplierList/>
    </div>
    </div>
  );
}

export default App;
